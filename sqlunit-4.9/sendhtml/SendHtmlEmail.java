import java.util.*;
import java.io.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

class SendHtmlEmail
{
   public static void main(String [] args) {

      try {
         System.out.println("Preparing to send....");
         Properties properties = new Properties();
         properties.load(new FileInputStream("mail.properties"));  

         Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
               return new PasswordAuthentication(properties.getProperty("from"),properties.getProperty("password"));
            }
         });

         String to = "";
         String cc = "";
         String[] recipients = properties.getProperty("recipients").toString().replaceAll(" ","").split(",");

         for(int i = 0; i < recipients.length; i++) {
            if(i==0) to += recipients[i];
            else cc += recipients[i] + ",";
         }

         Message message = new MimeMessage(session);
         message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
         if(cc.length()!=0) {
            cc = cc.substring(0,cc.length()-1);
            message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
         }
         message.setFrom(new InternetAddress(properties.getProperty("from")));
         message.setSubject(properties.getProperty("subject"));
         message.setDataHandler(new DataHandler(new FileDataSource("../output/mytests.html")));
         Transport.send(message);
         System.out.println("Message sent successfully!");
      } catch (Exception e) {
         System.out.println("Failed to send the message");
         e.printStackTrace();
      }

   }
}