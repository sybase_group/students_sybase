﻿# SQLUnit

## Unit Testing with SQLUnit
https://sqlunit.sourceforge.net/ <br />
https://sqlunit.sourceforge.net/book1.html

## SQLUnit version 4.9 is used in this project 

**For Help/Available Commands**

    ant
    #or
    ant help

**Build**

    ant install
 
**Run Tests**

    ant run-all-sybase-tests
    #or
    ant sqlunit-nested -Dtestdir=sybase/
    
## Generating HTML Test Report 
  
    ant -Dlog.format=canoo -Doutput.file=output/mytests.xml run-all-sybase-tests canoo2html 

OR

**Generate XML file for test results**

    ant -Dlog.format=canoo -Doutput.file=output/mytests.xml run-all-sybase-tests

**Convert XML file into HTML**

    ant canoo2html

