<?xml version="1.0" encoding="UTF-8"?>
<!-- <?xml-stylesheet type="text/xsl" href="C:\sqlunit-4.6\output\TestResults.xsl"?> -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <html>
      <head>
        <title>Results of SQLUnit Test Run</title>
      </head>
      <style>
      p {
        margin: 0;
      }
      </style>
      <body>
        <h1>Results of SQLUnit Test Run</h1>
        <div>
          <!-- ******************** -->
          <!-- Calculate statistics -->
          <!-- ******************** -->
          <!-- Total number of test cases -->
          <xsl:variable name="totalTestCases" select="count(//testresult)"/>
          <!-- Total number of failing test cases.  (At least one failing test constitutes a failure. -->
          <xsl:variable name="numTestCasesFail" select="count(//testresult/results/failure[1]) + count(//testresult/results/error[1])"/>
          <!-- Total number of passing test cases -->
          <xsl:variable name="numTestCasesPass" select="$totalTestCases - $numTestCasesFail"/>
          <!-- Total number of tests executed -->
          <xsl:variable name="totalTests" select="count(//step) + count(//failure) + count(//error)"/>
          <!-- Total number of failing tests -->
          <xsl:variable name="numTestsFail" select="count(//failure) + count(//error)"/>
          <!-- Total number of passing tests -->
          <xsl:variable name="numTestsPass" select="$totalTests - $numTestsFail"/>
          <xsl:variable name="summaryResult">
            <xsl:choose>
              <xsl:when test="$numTestCasesFail=0">&lt;span style="color:limegreen;"&gt;Success&lt;/span&gt;</xsl:when>
              <xsl:otherwise>&lt;span style="color:red;"&gt;Failure&lt;/span&gt;</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <h2>Summary of Test Results: <xsl:value-of select="$summaryResult" disable-output-escaping="yes"/>
          </h2>
          <table>
            <tbody>
              <tr>
                <td width="46%">
                  <table border="1">
                    <thead>
                      <tr>
                        <th colspan="3">Test Case Summary</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th width="33%">Total Executed</th>
                        <th width="33%">Passed</th>
                        <th width="33%">Failed</th>
                      </tr>
                      <tr>
                        <td align="center">
                          <xsl:value-of select="$totalTestCases"/>
                        </td>
                        <td align="center">
                          <xsl:value-of select="$numTestCasesPass"/>
                        </td>
                        <td align="center">
                          <xsl:value-of select="$numTestCasesFail"/>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td width="8%"/>
                <td width="46%">
                  <table border="1">
                    <thead>
                      <tr>
                        <th colspan="3">Test Summary</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th width="33%">Total Executed</th>
                        <th width="33%">Passed</th>
                        <th width="33%">Failed</th>
                      </tr>
                      <tr>
                        <td align="center">
                          <xsl:value-of select="$totalTests"/>
                        </td>
                        <td align="center">
                          <xsl:value-of select="$numTestsPass"/>
                        </td>
                        <td align="center">
                          <xsl:value-of select="$numTestsFail"/>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <br/>
          <table width="55%" border="1">
            <thead>
              <tr>
                <th colspan="4">Test Case Breakdown</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Test Case</th>
                <th>Total Tests</th>
                <th>Tests Passed</th>
                <th>Tests Failed</th>
              </tr>
              <xsl:for-each select="/summary/testresult">
                <xsl:variable name="totalTestsWithinCase" select="count(results/step) + count(results/failure)"/>
                <xsl:variable name="numTestsWithinTestCaseFail" select="count(results/failure)"/>
                <xsl:variable name="numTestsWithinTestCasePass" select="$totalTestsWithinCase - $numTestsWithinTestCaseFail"/>
                <tr>
                  <td>
                    <xsl:if test="$numTestsWithinTestCaseFail &gt; 0">
                      <xsl:text disable-output-escaping="yes">&lt;span style="color:red;"&gt;</xsl:text>
                    </xsl:if>
                    <xsl:if test="$numTestsWithinTestCaseFail &gt; 0">
                      <xsl:text disable-output-escaping="yes">&lt;/span&gt;</xsl:text>
                    </xsl:if>
                    <xsl:value-of select="substring-after(substring-before(@location,'.xml'),'test/')"/>
                  </td>
                  <td>
                    <xsl:value-of select="$totalTestsWithinCase"/>
                  </td>
                  <td>
                    <xsl:value-of select="$numTestsWithinTestCasePass"/>
                  </td>
                  <td>
                    <xsl:if test="$numTestsWithinTestCaseFail &gt; 0">
                      <xsl:text disable-output-escaping="yes">&lt;span style="color:red;"&gt;</xsl:text>
                    </xsl:if>
                    <xsl:value-of select="$numTestsWithinTestCaseFail"/>
                    <xsl:if test="$numTestsWithinTestCaseFail &gt; 0">
                      <xsl:text disable-output-escaping="yes">&lt;/span&gt;</xsl:text>
                    </xsl:if>
                  </td>
                </tr>
              </xsl:for-each>
            </tbody>
          </table>
          <br/>
          <xsl:if test="$numTestCasesFail &gt; 0">
            <h2>Test Failure Details</h2>
            <xsl:for-each select="/summary/testresult/results/failure">
              <strong>
                <xsl:value-of select="concat('test/',substring-after(substring-before(parent::node()/parent::node()/@location,'.xml'),'test/'))"/>
              </strong>
              <br/>
              <strong>
                sqlunit:test
              </strong>
              <p>
                <xsl:value-of select="substring-after(substring-before(@message,'***'),'SQLUnitException: ')"/>
              </p>
              <p>*** Expected:</p>
              <p>
                <xsl:value-of select="substring-before(substring-after(@message,'*** expected:'),'*')"/>
              </p>
              <p>*** But got:</p>
              <p>
                <xsl:value-of select="concat(substring-before(substring-after(@message,'*** but got:'),'/result>'),'/result>')"/>
              </p>
              <p>
                <xsl:value-of select="substring-after(substring-after(@message,'*** but got:'),'/result>')"/>
              </p>
              <hr/>
            </xsl:for-each>
          </xsl:if>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
