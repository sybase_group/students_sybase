Release notes for SQLUnit version 4.9
=====================================

Bugs fixed in this release
--------------------------
   - User defined types need to provide the type name in the registerOutParameter() call. The param tag has a new optional attribute typename to support this. Feature gap pointed out and fix provided by Greg Vatman.
   - Bug fix in matching outparams by id. The older (buggy) implementation used a serial number of the outparam generated, but the newer (correct according to the JDBC spec) uses the param id of the corresponding input param. Bug pointed out by James Henderson.
   - Include handler now respects the transactional property of the Connection object it uses. This bug was pointed out by James Henderson.

Changes in this release
-----------------------
    - SQLUnit now throws a full stack trace when it encounters an exception, regardless of whether its in debug mode or not. This functionality was requested by trcull.

New Features in this release
----------------------------
   - A SQLUnit test suite for Microsoft SQL Server contributed by James Henderson.
   - Tools contributed by James Henderson to convert test result output using the Canoo reporter to HTML.
   - New tags for test grouping (classifiers) and to skip tests (skip) contributed by Ivan Ivanov and Craeg Strong. These tags have also been applied to diff, batchtest and func so any type of test can be grouped into categories. Documentation for these tags have also been provided by Ivan Ivanov.
   - Two new Matchers contributed by Tim Cull.

TODO
----
   - make checkstyle clean
