# Screenshots

### Teams Notification
![Teams Notification](./CustomTeamsNotification.PNG)

### Test Results Mail
![Test Results Mail](./TestResultsMail.PNG)

### Test Failure Message
![Test Failure Message](./TestFailureMessage.PNG)

### Html Test Report
![Html Test Report](./HtmlTestReport.PNG)

### Teams Incoming Webhook
![Teams Incoming Webhook](./ConfigureTeamsIncomingWebhook.PNG)