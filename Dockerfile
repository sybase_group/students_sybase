FROM centos:centos7

COPY assets /opt/tmp/
COPY sqlunit-4.9 /opt/sybase/sqlunit-4.9

RUN cp /opt/tmp/sysctl.conf /etc/ && true || /sbin/sysctl -p \
 && yum install -q -y libaio unzip dos2unix ant \
 && mv /opt/tmp/init-scripts /opt/sybase/ \
 && dos2unix /opt/tmp/* \
 && find /opt/sybase/init-scripts -name "*.sql" | xargs dos2unix \
 && mkdir -p /opt/tmp/ASE_Suite \
 && tar xfz /opt/tmp/ASE_Suite.linuxamd64.tgz -C /opt/tmp/ASE_Suite \
#  && mv /opt/tmp/ASE_Suite/ebf29650/* /opt/tmp/ASE_Suite/ \
 && rm -rf /opt/tmp/ASE_Suite.linuxamd64.tgz \
 && /opt/tmp/ASE_Suite/setup.bin \
    -f /opt/tmp/sybase-response.txt \
    -i silent \
    -DAGREE_TO_SAP_LICENSE=true \
    -DRUN_SILENT=true \
 && cp /opt/tmp/sybase-ase.rs /opt/sybase/ASE-16_0/sybase-ase.rs \
 && source /opt/sybase/SYBASE.sh \
 && /opt/sybase/ASE-16_0/bin/srvbuildres -r /opt/sybase/ASE-16_0/sybase-ase.rs \
 && mv /opt/sybase/interfaces /opt/sybase/interfaces.backup \
 && cp /opt/tmp/interfaces /opt/sybase/ \
 && cp /opt/tmp/sybase-entrypoint.sh /usr/local/bin/ \
 && chmod +x /usr/local/bin/sybase-entrypoint.sh \
 && ln -s /usr/local/bin/sybase-entrypoint.sh /sybase-entrypoint.sh \
 && ant install -f opt/sybase/sqlunit-4.9/ \
 && find /opt/tmp/ -type f | xargs -L1 rm -f \
 && rm -rf /tmp/* \
 && rm -rf /opt/sybase/sqlunit-4.9/src \
 && rm -rf /opt/sybase/sqlunit-4.9/docs/javadoc \
 && rm -fr /opt/sybase/shared/SAPJRE-* \
 && rm -fr /opt/sybase/shared/ase/SAPJRE-* \
 && rm -fr /opt/sybase/jre64 \
 && rm -fr /opt/sybase/sybuninstall \
 && rm -fr /opt/sybase/jConnect-16_0 \
 && rm -fr /opt/sybase/jutils-3_0 \
 && rm -fr /opt/sybase/ASE-16_0/bin/diag* \
 && rm -fr /opt/sybase/OCS-16_0/devLib* \
 && rm -fr /opt/sybase/SYBDIAG \
 && rm -fr /opt/sybase/COCKPIT-4 \
 && rm -fr /opt/sybase/WS-16_0 \
 && rm -fr /opt/sybase/OCS-16_0/bin32 \
 && rm -fr /opt/sybase/OCS-16_0/lib3p \
 && rm -fr /opt/sybase/OCS-16_0/lib/*[!6][!4].a \
 && rm -fr /opt/sybase/OCS-16_0/lib/*[!6][!4].so \
 # remove graphical apps and java
 && cd /opt/sybase/ASE-16_0/bin && rm asecfg ddlgen *java sqlloc sqlupgrade srvbuild \
 && tar -czf /tmp/data.tar.gz /opt/sybase/data \
 && rm -fr /opt/sybase/data \
 && yum clean all \
 && rm -rf /var/cache/yum 

# Setup the ENV
RUN ["/bin/bash", "-c", "source /opt/sybase/SYBASE.sh"]

# ENTRYPOINT ["/sybase-entrypoint.sh"]

# EXPOSE 5000
