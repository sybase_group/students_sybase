package com.studentsAPI.studentsAPI.dao;

import com.studentsAPI.studentsAPI.entities.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseDao extends JpaRepository<Course, Integer> {

}
