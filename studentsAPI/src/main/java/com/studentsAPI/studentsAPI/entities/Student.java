package com.studentsAPI.studentsAPI.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="student")
@Entity
public class Student {
    @Id
    private int student_id;
    private String name;
    private int age;

    public Student() {

    }

    public Student(int student_id, String name, int age) {
        super();
        this.student_id = student_id;
        this.name = name;
        this.age = age;
    }


    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
