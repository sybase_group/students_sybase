package com.studentsAPI.studentsAPI.controller;

import com.studentsAPI.studentsAPI.entities.Course;
import com.studentsAPI.studentsAPI.entities.Student;
import com.studentsAPI.studentsAPI.service.ApiService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class ApiController {

    private ApiService apiService;

    public ApiController(ApiService apiService) {
        this.apiService = apiService;
    }

//    @Autowired
//    public DataController(DataService dataService) {
//        this.dataService = dataService;
//    }

    @GetMapping("/")
    public String greet() {
        return "MYSYBASE - students_db";
    }

    @GetMapping("/students")
    public List<Student> findStudents() {
        return this.apiService.getStudents();
    }

    @GetMapping("/students/{studentId}")
    public Student findStudents(@PathVariable String studentId) {
        return this.apiService.getStudent(Integer.parseInt(studentId));
    }

    @GetMapping("/courses")
    public List<Course> findCourses() {
        return this.apiService.getCourses();
    }

    @GetMapping("/courses/{userId}")
    public Course findCourses(@PathVariable String userId) {
        return this.apiService.getCourse(Integer.parseInt(userId));
    }

    @PostMapping("/students")
    public Student addStudent(@RequestBody Student student) {
        return this.apiService.addStudent(student);
    }

    @PostMapping("/courses")
    public Course addCourse(@RequestBody Course course) {
        return this.apiService.addCourse(course);
    }

    @PatchMapping("/students/{studentId}")
    public Student updateStudent(@PathVariable int studentId, @RequestBody Map<String, Object> fields) {
        return this.apiService.updateStudent(studentId, fields);
    }

    @PatchMapping("/courses/{courseId}")
    public Course updateCourse(@PathVariable int courseId, @RequestBody Map<String, Object> fields) {
        return this.apiService.updateCourse(courseId, fields);
    }

    @DeleteMapping("/students/{studentId}")
    public ResponseEntity<HttpStatus> deleteStudent(@PathVariable String studentId) {
        return this.apiService.deleteStudent(Integer.parseInt(studentId));
    }

    @DeleteMapping("/courses/{courseId}")
    public ResponseEntity<HttpStatus> deleteCourse(@PathVariable String courseId) {
        return this.apiService.deleteCourse(Integer.parseInt(courseId));
    }

}
