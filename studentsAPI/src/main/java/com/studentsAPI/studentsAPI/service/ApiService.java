package com.studentsAPI.studentsAPI.service;

import com.studentsAPI.studentsAPI.entities.Course;
import com.studentsAPI.studentsAPI.dao.CourseDao;
import com.studentsAPI.studentsAPI.entities.Student;
import com.studentsAPI.studentsAPI.dao.StudentDao;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ApiService {
    
    private StudentDao studentDao;

    private CourseDao courseDao;

    public ApiService(StudentDao studentDao, CourseDao courseDao) {
        this.studentDao = studentDao;
        this.courseDao = courseDao;
    }

    public List<Student> getStudents() {
        return studentDao.findAll();
    }

    public Student getStudent(int parseInt) {
        return studentDao.findById(parseInt).isPresent() ? studentDao.findById(parseInt).get() : null;
    }

    public List<Course> getCourses() {
        return courseDao.findAll();
    }

    public Course getCourse(int parseInt) {
        return courseDao.findById(parseInt).isPresent() ? courseDao.findById(parseInt).get() : null;
    }

    public Student addStudent(Student student) { return studentDao.save(student); }

    public Course addCourse(Course course) {
        return courseDao.save(course);
    }

    public Student updateStudent(int studentId, Map<String, Object> fields) {
        Optional<Student> existingStudent= studentDao.findById(studentId);

        if (existingStudent.isPresent()){
            fields.forEach((key,value) -> {
                Field field = ReflectionUtils.findField(Student.class, key);
                assert field != null;
                field.setAccessible(true);
                ReflectionUtils.setField(field, existingStudent.get(), value);
            });
            return studentDao.save(existingStudent.get());
        }
        return null;
    }

    public Course updateCourse(int courseId, Map<String, Object> fields) {
        Optional<Course> existingCourse= courseDao.findById(courseId);

        if (existingCourse.isPresent()){
            fields.forEach((key,value) -> {
                Field field = ReflectionUtils.findField(Course.class, key);
                assert field != null;
                field.setAccessible(true);
                ReflectionUtils.setField(field, existingCourse.get(), value);
            });
            return courseDao.save(existingCourse.get());
        }
        return null;
    }

    public ResponseEntity<HttpStatus> deleteStudent(int parseInt) {
        try {
            studentDao.delete(studentDao.findById(parseInt).get());
            System.out.println("Deleted");
            return new ResponseEntity<>(HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
}

    public ResponseEntity<HttpStatus> deleteCourse(int parseInt) {
        try {
            courseDao.delete(courseDao.findById(parseInt).get());
            System.out.println("Deleted");
            return new ResponseEntity<>(HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
