package com.studentsAPI.studentsAPI;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.studentsAPI.studentsAPI.controller.ApiController;
import com.studentsAPI.studentsAPI.entities.Course;
import com.studentsAPI.studentsAPI.entities.Student;
import com.studentsAPI.studentsAPI.service.ApiService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ApiController.class)
@AutoConfigureMockMvc(addFilters = false)
class StudentsApiApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ApiService apiService;

	Student mockStudent1 = new Student(1,"Bob",30);
	Student mockStudent2 = new Student(2,"Jack",20);
	List<Student> mockStudents = Arrays.asList(mockStudent1,mockStudent2);

	Course mockCourse1 = new Course(100,"Python Course");
	Course mockCourse2 = new Course(200, "React Course");
	List<Course> mockCourses = Arrays.asList(mockCourse1,mockCourse2);

	ObjectMapper om = new ObjectMapper();

	@Test
	public void retrieveAllStudents() throws Exception {
		Mockito.when(apiService.getStudents()).thenReturn(mockStudents);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/students").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String expected = om.writeValueAsString(Arrays.asList(mockStudent1,mockStudent2));

		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

	@Test
	public void retrieveAllCourses() throws Exception {
		Mockito.when(apiService.getCourses()).thenReturn(mockCourses);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/courses")
				.accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String expected = om.writeValueAsString(Arrays.asList(mockCourse1,mockCourse2));

		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

	@Test
	public void retrieveStudent() throws  Exception {
		Mockito.when(apiService.getStudent(Mockito.anyInt())).thenReturn(mockStudent1);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/students/1")
				.accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String expected = om.writeValueAsString(mockStudent1);

		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

	@Test
	public void retrieveCourse() throws  Exception {
		Mockito.when(apiService.getCourse(Mockito.anyInt())).thenReturn(mockCourse1);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/courses/100")
				.accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String expected = om.writeValueAsString(mockCourse1);

		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

	Student newStudent = new Student(3, "Paxton", 40);
	@Test
	public void addingStudent() throws  Exception {
		Mockito.when(apiService.addStudent(Mockito.any(Student.class))).thenReturn(newStudent);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/students")
				.accept(MediaType.APPLICATION_JSON)
				.content(om.writeValueAsString(newStudent))
				.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(status().isOk()).andReturn();
		String expected = om.writeValueAsString(newStudent);

		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

	Course newCourse = new Course(300, "Angular Course");
	@Test
	public void addingCourse() throws  Exception {
		Mockito.when(apiService.addCourse(Mockito.any(Course.class))).thenReturn(newCourse);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/courses")
				.accept(MediaType.APPLICATION_JSON)
				.content(om.writeValueAsString(newCourse))
				.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(status().isOk()).andReturn();
		String expected = om.writeValueAsString(newCourse);

		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

	@Test
	public void updateStudent() throws Exception {
		Mockito.when(apiService.updateStudent(Mockito.anyInt(),Mockito.anyMap())).thenReturn(mockStudent1);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.patch("/students/2")
				.accept(MediaType.APPLICATION_JSON)
				.content("{\"age\":22}")
				.contentType(MediaType.APPLICATION_JSON);
		ResultActions result = mockMvc.perform(requestBuilder);
		result.andExpect(status().isOk());
	}

	@Test
	public void updateCourse() throws Exception {
		Mockito.when(apiService.updateCourse(Mockito.anyInt(),Mockito.anyMap())).thenReturn(mockCourse1);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.patch("/courses/100")
				.accept(MediaType.APPLICATION_JSON)
				.content("{\"name\":\"Java\"}")
				.contentType(MediaType.APPLICATION_JSON);
		ResultActions result = mockMvc.perform(requestBuilder);
		result.andExpect(status().isOk());
	}

	@Test
	public void removeStudent() throws Exception {
		Mockito.when(apiService.deleteStudent(Mockito.anyInt()))
				.thenReturn(new ResponseEntity<>(HttpStatus.OK));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/students/" + Mockito.anyInt())
				.accept(MediaType.APPLICATION_JSON);
		ResultActions result = mockMvc.perform(requestBuilder);
		result.andExpect(status().isOk());
	}

	@Test
	public void removeCourse() throws Exception {
		Mockito.when(apiService.deleteCourse(Mockito.anyInt()))
				.thenReturn(new ResponseEntity<>(HttpStatus.OK));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/courses/" + Mockito.anyInt())
				.accept(MediaType.APPLICATION_JSON);
		ResultActions result = mockMvc.perform(requestBuilder);
		result.andExpect(status().isOk());
	}
}
