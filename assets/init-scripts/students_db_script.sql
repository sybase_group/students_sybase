create database students_db
go
use students_db
go
create table student (
student_id int primary key,
name varchar(20),
age int
)
go
create table course (
department_id int primary key,
name varchar(30)
)
go
create table enrolment (
student_id int references student(student_id),
department_id int references course(department_id),
primary key(student_id, department_id)
)
go
insert into student values(101,'Akash',25)
go
insert into student values(102,'Sidhant',22)
go
insert into student values(103,'Max',19)  
go
insert into course values(1,'Java')              
go
insert into course values(2,'Scala')
go
insert into enrolment values(101,1)  
go
insert into enrolment values(101,2)
go
insert into enrolment values(102,1)
go
create view course1students as
select student.name from student
where student.student_id in (
select enrolment.student_id from enrolment
where enrolment.department_id = 1
)
go
create procedure students_in_course1 as
select * from course1students
go
