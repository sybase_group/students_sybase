# --- This file was generated by Sybase Installer ---
#
sybinit.release_directory: /opt/sybase
sybinit.product: sqlsrv
sqlsrv.server_name: MYSYBASE
sqlsrv.sa_password: myPassword
sqlsrv.new_config: yes
sqlsrv.do_add_server: yes
sqlsrv.network_protocol_list: tcp
sqlsrv.network_hostname_list: localhost
sqlsrv.network_port_list: 5000
sqlsrv.application_type: MIXED
sqlsrv.server_page_size: 4k
sqlsrv.force_buildmaster: no
sqlsrv.master_device_physical_name: /opt/sybase/data/master.dat
sqlsrv.master_device_size: 78
sqlsrv.master_database_size: 39
sqlsrv.errorlog: /opt/sybase/ASE-16_0/install/MYSYBASE.log
sqlsrv.do_upgrade: no
sqlsrv.sybsystemprocs_device_physical_name: /opt/sybase/data/sysprocs.dat
sqlsrv.sybsystemprocs_device_size: USE_DEFAULT
sqlsrv.sybsystemprocs_database_size: USE_DEFAULT
sqlsrv.sybsystemdb_device_physical_name: /opt/sybase/data/sybsysdb.dat
sqlsrv.sybsystemdb_device_size: USE_DEFAULT
sqlsrv.sybsystemdb_database_size: USE_DEFAULT
sqlsrv.tempdb_device_physical_name: /opt/sybase/data/tempdbdev.dat
sqlsrv.tempdb_device_size: USE_DEFAULT
sqlsrv.tempdb_database_size: USE_DEFAULT
sqlsrv.default_backup_server: MYSYBASE_BS
# Additional param to dataserver cmd to skip some check that make server crash
# https://answers.sap.com/questions/13386863/i-updated-my-system-and-now-server-is-crashing.html
sqlsrv.addl_cmdline_parameters: -T11889
sqlsrv.do_configure_pci: no
# If sqlsrv.do_optimize_config is set to yes, bot sqlsrv.avail_physical_memory and sqlsrv.avail_cpu_num need to be set.
sqlsrv.do_optimize_config: no
sqlsrv.avail_physical_memory: 256
sqlsrv.avail_cpu_num: 1
sqlsrv.default_language: english
sqlsrv.default_characterset: utf8
sqlsrv.characterset_install_list: cp437,utf8
sqlsrv.configure_remote_command_and_control_agent_ase: no
sqlsrv.enable_ase_for_ase_cockpit_monitor: no