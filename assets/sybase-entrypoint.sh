#! /bin/bash

# Source in the Sybase environment variables
echo "---ENTRYPOINT START---" > /tmp/sybase-entrypoint.log
source /opt/sybase/SYBASE.sh
echo "source SYBASE.sh" >> /tmp/sybase-entrypoint.log

ret=0
# if database doesn't exist, we create it from out premade package
if [ ! -f "/opt/sybase/data/master.dat" ]; then
    cd /
    tar -xzf /tmp/data.tar.gz --no-same-owner
    ret=$?
    cd -
fi

if [ $ret -ne 0 ]; then
    echo "Error while extracting database files. Exiting..."
    exit 1
fi

# Move old logs aside
mv /opt/sybase/ASE-16_0/install/MYSYBASE.log /opt/sybase/ASE-16_0/install/MYSYBASE.log.old
echo "mv MYSYBASE.log" >> /tmp/sybase-entrypoint.log

# Start QIPSYBASE
echo "run RUN_MYSYBASE" >> /tmp/sybase-entrypoint.log
${SYBASE}/${SYBASE_ASE}/install/RUN_MYSYBASE &
sleep 15

# time=0
# i_status=0
#   echo "Waiting for init finish - time: $time  i_status: $i_status" >> /tmp/sybase-entrypoint.log
# while [[ $i_status -eq 0 ]] && [[ $time -lt 15 ]]
# do 
#   sleep 1
#   time=$((time+1))
#   i_status= $(grep "Console logging is disabled." /opt/sybase/ASE-16_0/install/MYSYBASE.log | wc -l)
#   echo "Waiting for init finish - time: $time  i_status: $i_status" >> /tmp/sybase-entrypoint.log
#   echo "Waiting for init finish - time: $time  i_status: $i_status"
# done

echo "init-scripts loop start" >> /tmp/sybase-entrypoint.log
for f in /opt/sybase/init-scripts/*.sql
do
  if [[ -f "$f" ]]
  then 
    echo "init-scripts: $f" >> /tmp/sybse-entrypoint.log
    # dos2unix $f
    source /opt/sybase/SYBASE.sh && isql -SMYSYBASE -Dmaster -Usa -PmyPassword -i$f >> /tmp/sybase-entrypoint.log
    echo "Executed $f"
  fi
done

sleep 5

#trap
# echo "before trap" >> /tmp/sybase-entrypoint.log
# while [ "$END" == '' ]; do
#       sleep 1
# #     trap "/etc/init.d/sybase stop && END=1" INT TERM
# done
# echo "after trap" >> /tmp/sybase-entrypoint.log
